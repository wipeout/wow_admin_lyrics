<?php
	/**
	 * @package wow_admin_Lyrics
	 * @version 1.0
	 */
	/*
	Plugin Name: WipeOut! random admin lyrics
	Plugin URI: http://www.wipeout.nu/wordpress
	Description: In a similar vein to the "Hello Dolly" plugin, you can get your daily fix of lyrics from Dave Matthews Band songs right up in your admin header.
	Author: Dennis Lutz
	Version: 1.0
	Author URI: http://www.wipeout.nu/
	*/


	function wow_admin_get_lyric( $artist = "dave matthews band" ) {
		$url    = "http://music.wipeout.nu/randomlyrics/?artist={$artist}";
		$lyrics = file_get_contents( $url );
		$lyrics = json_decode( $lyrics, true );

		return ( $lyrics );
	}

	function wow_admin_lyrics() {
		$chosenDMB = wow_admin_get_lyric();
		echo '<p id="wow_admin_lyrics" title="' . $chosenDMB['title'] . '-' . $chosenDMB['artist'] . '">' . $chosenDMB['randomline'] . '</p>';
	}


	function wow_admin_lyrics_css() {

		$x = is_rtl() ? 'left' : 'right';

		echo "
    <style>
        #wow_admin_lyrics {
            float: $x;
            padding-$x: 15px;
            padding-top: 5px;       
            margin: 0;
            font-size: 11px;
            cursor: pointer;
        }
    </style>
    ";
	}

	function wow_enqueue() {
		wp_enqueue_script( 'wow_admin_lyrics_script', plugin_dir_url( __FILE__ ) . '/script.js',false );
	}


	add_action( 'admin_notices', 'wow_admin_lyrics' );
	add_action( 'admin_head', 'wow_admin_lyrics_css' );
	add_action( 'admin_enqueue_scripts', 'wow_enqueue' );



