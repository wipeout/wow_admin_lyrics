/**
 * Created by Dennis on 19-5-2015.
 */

jQuery( document ).ready(function() {
	loadLyrics();
	jQuery('body').on('click','#wow_admin_lyrics',function(){
		loadLyrics();
	});
});

function loadLyrics()
{
	var artist =  '';
	var url    = "http://music.wipeout.nu/randomlyrics/";

	jQuery.ajax({
		url: url,
		dataType: 'jsonp',
		data: { artist: artist },
		success: function(e){
			var wowdiv     = jQuery('#wow_admin_lyrics');
			var randomline = e[0].randomline;
			var title      = e[0].title;
			var artist     = e[0].artist;
			wowdiv.html(randomline).prop('title',title + ' - ' + artist );
		},
		error: function(e){
			console.log('er ging iets mis');
			console.log(e);
		}
	});
}